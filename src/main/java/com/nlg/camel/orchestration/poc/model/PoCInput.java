package com.nlg.camel.orchestration.poc.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/*
 * Fast & lazy way to JSON annotate a class. For a production system make 
 * sure you DO NOT use this method.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PoCInput {
	private String correlationId;
	private boolean failRequest;
	private String testId;
	private int sleepSeconds;

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public boolean getFailRequest() {
		return failRequest;
	}

	public void setFailRequest(boolean failRequest) {
		this.failRequest = failRequest;
	}

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	public int getSleepSeconds() {
		return sleepSeconds;
	}

	public void setSleepSeconds(int sleepSeconds) {
		this.sleepSeconds = sleepSeconds;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PoCInput [correlationId=");
		builder.append(correlationId);
		builder.append(", failRequest=");
		builder.append(failRequest);
		builder.append(", testId=");
		builder.append(testId);
		builder.append(", sleepSeconds=");
		builder.append(sleepSeconds);
		builder.append("]");
		return builder.toString();
	}
}
