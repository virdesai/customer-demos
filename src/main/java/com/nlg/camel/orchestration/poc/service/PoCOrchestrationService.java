package com.nlg.camel.orchestration.poc.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.nlg.camel.orchestration.poc.model.PoCInput;
import com.nlg.camel.orchestration.poc.model.PoCOutput;

@Path("/")
public interface PoCOrchestrationService {

	@POST
	@Path("/doSomething")
	@Consumes("application/json")
	@Produces("application/xml")
	PoCOutput doSomething(PoCInput pocInput);

}
