package com.nlg.camel.orchestration.poc.service;

import org.dozer.Mapper;

import com.nlg.camel.orchestration.poc.model.PoCInput;
import com.nlg.camel.orchestration.poc.model.PoCOutput;

public class PoCOrchestrationServiceImpl implements PoCOrchestrationService {
	private Mapper mapper;

	@Override
	public PoCOutput doSomething(PoCInput pocInput) {
		return mapper.map(pocInput, PoCOutput.class);
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}
}
