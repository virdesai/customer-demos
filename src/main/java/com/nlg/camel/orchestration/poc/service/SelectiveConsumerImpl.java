package com.nlg.camel.orchestration.poc.service;

import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Using constructor injection because this class depends on it.
 * 
 * Note: @Consume annotation did not inject the ConsumerTemplate.
 * 
 * Since this class only has one method and uses camel annotation to map the
 * parameter there is not need to explicitly specify the method inside the camel
 * context file.
 * 
 */
public class SelectiveConsumerImpl {
	@SuppressWarnings("unused")
	private static final Logger LOG = LoggerFactory.getLogger(SelectiveConsumerImpl.class);

	private static final String AMQ_SELECTOR_URI = "activemq:queue:nlg.poc.orchestration?selector=JMSCorrelationID='";

	private ConsumerTemplate consumerTemplate;

	public SelectiveConsumerImpl(ConsumerTemplate consumerTemplate) {
		this.consumerTemplate = consumerTemplate;
	}

	/*
	 * Retrieves message from the queue with matching correlation ID.
	 */
	public String getMsg(@Header("JMSCorrelationID") String id) {
		return this.consumerTemplate.receiveBody(AMQ_SELECTOR_URI + id + "'", String.class);
	}
}
